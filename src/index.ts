import { GraphQLServer } from 'graphql-yoga'
import { devConnection, prodConnection } from './connections'

import schema from './schema'
import { Youtuber } from './entity/Youtubers'
import { Categorie } from './entity/Categories'

async function main() {
  const prod = process.env.NODE_ENV === 'production'

  if (prod) {
    await prodConnection('dist')
  } else {
    await devConnection()
  }

  const port = process.env.PORT || 4000
  const server = new GraphQLServer({
    schema,
    context: (ctx) => ({
      req: ctx.request,
      Youtuber,
      Categorie,
    }),
  })
  // if (prod) {
  //   server.express.listen(port, '0.0.0.0', () => console.log(`Server is running on http://0.0.0.0:${port}`))
  // } else {
  server.start(
    {
      port,
    },
    () => console.log(`Server is running on http://localhost:${port}`)
  )
  // }
}

main()
