import { SchemaDirectiveVisitor } from 'graphql-tools'
import { GraphQLField, defaultFieldResolver } from 'graphql'
// import { Context } from '../utils'

export class AuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: GraphQLField<any, any>) {
    const { resolve = defaultFieldResolver } = field
    field.resolve = (...args) => {
      // console.log(args[2])
      return resolve.apply(this, args)
      // if (args[2].headers.authorization && args[2].headers.authorization === process.env.ENDPOINT_PASSWORD) {
      //   return resolve.apply(this, args)
      // } else {
      //   throw new Error('Not authorized.')
      // }
    }
  }
}

export class DelayDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: GraphQLField<any, any>) {
    const { resolve = defaultFieldResolver } = field
    field.resolve = (...args) => {
      return new Promise((r) => {
        setTimeout(() => {
          r(resolve.apply(this, args))
        }, this.args.time)
      })
    }
  }
}
