import fetch from 'node-fetch'

// import { Youtuber } from '../entity/Youtubers'
// import { Categorie } from '../entity/Categories'

import { Resolvers, reqPlayListItemlBaseUrl, reqVideoBaseUrl, ValideInput, reqChannelBaseUrl } from '../utils/utils'

class YoutubersError extends Error {
  name = 'YoutubersError'
  constructor() {
    super('No more youtubers to validate !')
  }
}

const resolvers: Resolvers = {
  Query: {
    version: () => '1.2.1',
    getRandomYoutuber: async (p, a, { Youtuber }) => {
      const ytb = await Youtuber.findOne({ where: { VALIDATE: 0 } })
      if (!ytb) {
        throw new YoutubersError()
      }
      return ytb
    },
    getCategories: async (p, a, { Categorie }) => Categorie.find(),
    getChannelSnippet: async (_, { id }: { id: string }) => {
      const reqUrl = reqChannelBaseUrl()
      reqUrl.searchParams.append('part', 'snippet')
      reqUrl.searchParams.append('id', id)

      const res = await fetch(reqUrl.toString())
      const data = await res.json()

      if (data.pageInfo.totalResults !== 1) {
        throw new Error('Invalid ID provided.')
      }

      const snippet = data.items[0].snippet

      return {
        title: snippet.title,
        description: snippet.description,
        thumbnails: snippet.thumbnails.high.url,
      }
    },
    getChannelVideos: async (_, { id }: { id: string }) => {
      const reqUrlChannel = reqChannelBaseUrl()
      reqUrlChannel.searchParams.append('part', 'contentDetails')
      reqUrlChannel.searchParams.append('id', id)

      const resChannel = await fetch(reqUrlChannel.toString())
      const dataChannel = await resChannel.json()

      if (dataChannel.pageInfo.totalResults !== 1) {
        throw new Error('Invalid ID provided.')
      }

      const uploads = dataChannel.items[0].contentDetails.relatedPlaylists.uploads

      const reqUrlPlaylistItems = reqPlayListItemlBaseUrl()
      reqUrlPlaylistItems.searchParams.append('part', 'contentDetails')
      reqUrlPlaylistItems.searchParams.append('playlistId', uploads)
      reqUrlPlaylistItems.searchParams.append('maxResults', '10')

      const resPlaylist = await fetch(reqUrlPlaylistItems.toString())
      const dataPlaylist = await resPlaylist.json()

      if (dataPlaylist.pageInfo.totalResults === 0) {
        throw new Error("This channel don't have any videos...")
      }

      const vidsId = dataPlaylist.items.map((item) => item.contentDetails.videoId)

      const videos = vidsId.map(async (vidId) => {
        const reqUrlVideo = reqVideoBaseUrl()
        reqUrlVideo.searchParams.append('part', 'statistics,snippet')
        reqUrlVideo.searchParams.append('id', vidId)

        const resVideo = await fetch(reqUrlVideo.toString())
        const dataVideo = await resVideo.json()

        const video = dataVideo.items[0]

        return {
          id: vidId,
          title: video.snippet.title,
          description: video.snippet.description,
          publishedAt: video.snippet.publishedAt,
          thumbnails: video.snippet.thumbnails.medium.url,
          url: `https://www.youtube.com/watch?v=${vidId}`,
          views: video.statistics.viewCount,
          likes: video.statistics.likeCount,
          dislikes: video.statistics.dislikeCount,
        }
      })

      return videos
    },
  },
  Mutation: {
    validateYoutuber: async (_, { input: args }: ValideInput, { Youtuber, Categorie }, info) => {
      const ytb = await Youtuber.findOne(args.uuid)
      if (!ytb) {
        throw new Error('No youtuber found...')
      }

      if (args.VALIDATE === false) {
        ytb.VALIDATE = 1
        ytb.save()
        return ytb
      }

      const CAT1 = await Categorie.findOne(args.CAT1)
      if (!CAT1) {
        throw new Error(`No categorie found for id ${args.CAT1}...`)
      }
      if (args.CAT2) {
        const CAT2 = await Categorie.findOne(args.CAT2)
        if (!CAT2) {
          throw new Error(`No categorie found for id ${args.CAT2}...`)
        }
        ytb.CAT2 = CAT2
      }

      ytb.VALIDATE = 2
      ytb.CAT1 = CAT1
      args.SOUSCAT1 && (ytb.SOUSCAT1 = args.SOUSCAT1)
      args.SOUSCAT2 && (ytb.SOUSCAT2 = args.SOUSCAT2)
      ytb.SEXE = args.FEMALE === true ? 'féminin' : args.FEMALE === false ? 'masculin' : ytb.SEXE

      await ytb.save()

      return ytb
    },
  },
}

export default resolvers
