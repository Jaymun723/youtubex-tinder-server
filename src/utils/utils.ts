import { Request } from 'express'
import { URL } from 'url'
import { Youtuber } from '../entity/Youtubers'
import { Categorie } from '../entity/Categories'

export type Resolvers = {
  [x: string]: {
    [x: string]: (parent: any, args: any, context: Context, info: any) => any
  }
}

export type Context = { req: Request; Youtuber: typeof Youtuber; Categorie: typeof Categorie }

export interface ValideInput {
  input: {
    uuid: string
    VALIDATE: boolean
    CAT1?: number
    SOUSCAT1?: string
    CAT2?: number
    SOUSCAT2?: string
    FEMALE?: boolean
  }
}

export const reqChannelBaseUrl = () => {
  const url = new URL('https://www.googleapis.com/youtube/v3/channels')
  url.searchParams.append('key', process.env.YOUTUBE_API_KEY)
  return url
}

export const reqPlayListItemlBaseUrl = () => {
  const url = new URL('https://www.googleapis.com/youtube/v3/playlistItems')
  url.searchParams.append('key', process.env.YOUTUBE_API_KEY)
  return url
}

export const reqVideoBaseUrl = () => {
  const url = new URL('https://www.googleapis.com/youtube/v3/videos')
  url.searchParams.append('key', process.env.YOUTUBE_API_KEY)
  return url
}
