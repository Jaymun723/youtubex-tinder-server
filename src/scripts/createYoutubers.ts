import * as fs from 'fs'
import * as path from 'path'
import { EventEmitter } from 'events'

import * as Papa from 'papaparse'
import { Youtuber } from '../entity/Youtubers'
import { prodConnection, devConnection } from '../connections'

import * as dotEnv from 'dotenv'

async function main() {
  if (process.argv[2] === 'prod') {
    dotEnv.config({ path: '.env.prod' })
    await prodConnection('src', '.ts')
  } else {
    await devConnection()
  }

  const file = fs.readFileSync(path.resolve(__dirname, '../../data/chaine.csv'), { encoding: 'UTF8' })

  Papa.parse(file, {
    complete: function({ data }) {
      const emiter = new EventEmitter()
      const youtubers = data.slice(1, data.length)
      // const youtubers = data.slice(1, 10)

      let savedYtb = 0

      youtubers.forEach((youtuber) => {
        const ytb = new Youtuber()
        ytb.URLID = youtuber[0]
        youtuber[1].length !== 0 && (ytb.URLPSEUDO = youtuber[1])
        ytb.URLWIZDEO = youtuber[2]
        youtuber[3].length !== 0 && (ytb.IDWIZDEO = youtuber[3])

        ytb
          .save()
          .then(() => emiter.emit('save'))
          .catch((err) => console.error('Error !'))
      })

      emiter.on('save', () => {
        savedYtb++
        if (savedYtb + 1 === youtubers.length) {
          console.log('Finished !')
          process.exit(0)
        }
      })
    },
  })
}

main()
