import * as fs from 'fs'
import * as path from 'path'
import { EventEmitter } from 'events'

import * as Papa from 'papaparse'

import { Categorie } from './../entity/Categories'
import { prodConnection, devConnection } from '../connections'

import * as dotEnv from 'dotenv'

async function main() {
  const emiter = new EventEmitter()

  if (process.argv[2] === 'prod') {
    dotEnv.config({ path: '.env.prod' })
    await prodConnection('src', '.ts')
  } else {
    await devConnection()
  }

  const file = fs.readFileSync(path.resolve(__dirname, '../../data/categorie.csv'), { encoding: 'UTF8' })

  let savedCat = 0

  Papa.parse(file, {
    complete: function({ data }) {
      let categories = []
      data.forEach((categorie) => {
        categories.push(categorie[1])
      })
      categories = categories.filter(function(item, pos) {
        return categories.indexOf(item) == pos
      })

      categories.forEach((categorie) => {
        const cat = new Categorie()
        cat.NAME = categorie
        cat
          .save()
          .then(() => emiter.emit('save'))
          .catch(console.error)
      })

      emiter.on('save', () => {
        savedCat++
        if (savedCat + 1 === categories.length) {
          console.log('Finish !')
          process.exit(0)
        }
      })
    },
  })
}

main()
