// import { join } from 'path'
// import * as fs from 'fs'

import { makeExecutableSchema } from 'graphql-tools'
// import { importSchema } from 'graphql-import'

import resolvers from './resolvers'
import { AuthDirective, DelayDirective } from './resolvers/schemaDirectives'

const gql = String.raw

const typeDefs = gql`
directive @auth on FIELD_DEFINITION
directive @delay(time: Int) on FIELD_DEFINITION

"""
_Relatif à l'api de YoutubeDex_
"""
type Youtuber {
  """
  UUID de youtuber dans la base de données
  """
  uuid: String!

  """
  0 => pas controllé, 1 => pas valide, 2 => valide
  """
  VALIDATE: Int!

  URLID: String!
  URLPSEUDO: String
  URLWIZDEO: String!
  IDWIZDEO: String

  CAT1: Categorie
  SOUSCAT1: String

  CAT2: Categorie
  SOUSCAT2: String

  SEXE: String
}

"""
_Relatif à l'api de YoutubeDex_
"""
type Categorie {
  """
  ID de la catégorie dans la base de données (ex: 1)
  """
  ID: Int!
  NAME: String!
  YOUTUBERS: [Youtuber]
}

"""
_Relatif à l'api de Youtube_
"""
type ChannelSnippet {
  title: String!
  description: String!
  thumbnails: String!
}

"""
_Relatif à l'api de Youtube_
"""
type VideoSnippet {
  id: String!
  title: String!
  description: String!
  publishedAt: String!
  thumbnails: String!
  url: String!
  views: Int!
  likes: Int!
  dislikes: Int!
}

"""
'Input' pour valider ou pas un youtuber.
_Relatif à l'api de YoutubeDex_
"""
input ValideInput {
  """
  Uuid de youtuber à valider
  """
  uuid: String!
  VALIDATE: Boolean!

  """
  Id de la première catégorie
  """
  CAT1: Int
  SOUSCAT1: String

  """
  Id de la deuxième catégorie
  """
  CAT2: Int
  SOUSCAT2: String

  FEMALE: Boolean
}

type Query {
  version: String!
  """
  Retourne un youtuber non validé au hasard.

  **Requière un header 'Authorization' correct**

  _Relatif à l'api de YoutubeDex_
  """
  getRandomYoutuber: Youtuber! @auth
  """
  **Requière un header 'Authorization' correct**

  _Relatif à l'api de YoutubeDex_
  """
  getCategories: [Categorie!]! @auth
  """
  Retourne des informations sur une Chaine Youtube.

  _Relatif à l'api de Youtube_
  """
  getChannelSnippet(id: String!): ChannelSnippet!
  """
  Retourne les 10 dernières vidéos d'une Chaine Youtube.

  _Relatif à l'api de Youtube_
  """
  getChannelVideos(id: String!): [VideoSnippet!]!
}

type Mutation {
  """
  **Requière un header 'Authorization' correct**

  _Relatif à l'api de YoutubeDex_
  """
  validateYoutuber(input: ValideInput!): Youtuber! @auth
}
`

export default makeExecutableSchema({
  resolvers,
  typeDefs,
  schemaDirectives: { auth: AuthDirective, delay: DelayDirective },
})
