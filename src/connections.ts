import { createConnection } from 'typeorm'
import { join } from 'path'

export const devConnection = async () => {
  console.log('dev')
  return createConnection({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'toto',
    database: 'youtubedex-db',
    logging: true,
    synchronize: true,
    entities: ['src/entity/**/*.*s'],
    // entities: [join(process.cwd() + 'src/entity/**/*.ts')],
    // migrations: [join(process.cwd() + 'src/migration/**/*.ts')],
    // subscribers: [join(process.cwd() + 'src/subscriber/**/*.ts')],
  }).catch(console.error)
}

export const prodConnection = async (path?: string, ext?: string) => {
  console.log('prod')
  console.log(process.cwd())
  return createConnection({
    type: 'mysql',
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    logging: true,
    synchronize: true,
    entities: [
      path ? `${path}/entity/**/*${ext ? ext : '.js'}` : join(process.cwd() + `/entity/**/*${ext ? ext : '.js'}`),
    ],
    migrations: [
      path ? `${path}/entity/**/*${ext ? ext : '.js'}` : join(process.cwd() + `/migration/**/*${ext ? ext : '.js'}`),
    ],
    subscribers: [
      path ? `${path}/entity/**/*${ext ? ext : '.js'}` : join(process.cwd() + `/subscriber/**/*${ext ? ext : '.js'}`),
    ],
    connectTimeout: 1000000,
  }).catch((e) => {
    console.error(e)
    process.exit(1)
  })
}
