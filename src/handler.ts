import { APIGatewayProxyHandler } from './utils/aws'
import { GraphQLServerLambda } from 'graphql-yoga'

import { Youtuber } from './entity/Youtubers'
import { Categorie } from './entity/Categories'

import schema from './schema'
import { devConnection, prodConnection } from './connections'

const lambda = new GraphQLServerLambda({
  schema,
  context: (ctx) => ({
    req: ctx.request,
    Youtuber,
    Categorie,
  }),
})

export const graphqlHandler: APIGatewayProxyHandler = (event, context, callback) => {
  if (process.env.NODE_ENV === 'production') {
    prodConnection().then(() => lambda.graphqlHandler(event, context, callback))
  } else {
    devConnection().then(() => lambda.graphqlHandler(event, context, callback))
  }
}

export const playgroundHandler = lambda.playgroundHandler
// export const graphqlHandler = lambda.graphqlHandler
