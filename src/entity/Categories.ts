import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from 'typeorm'
import { Youtuber } from './Youtubers'

@Entity()
export class Categorie extends BaseEntity {
  @PrimaryGeneratedColumn() ID: number

  @Column({ unique: true })
  NAME: string

  @OneToMany((type) => Youtuber, (youtuber) => youtuber.CAT1)
  @OneToMany((type) => Youtuber, (youtuber) => youtuber.CAT2)
  YOUTUBERS: Youtuber[]
}
