import { Entity, PrimaryColumn, Column, Generated, BaseEntity, ManyToOne } from 'typeorm'
import { Categorie } from './Categories'

@Entity()
export class Youtuber extends BaseEntity {
  @PrimaryColumn({ nullable: false, unique: true })
  @Generated('uuid')
  uuid: string

  // 0 => no set
  // 1 => no valid
  // 2 => valid
  @Column('int', { default: 0 })
  VALIDATE: number

  @Column({ unique: true })
  URLID: string

  @Column({ unique: true, nullable: true })
  URLPSEUDO: string

  @Column({ unique: true })
  URLWIZDEO: string

  @Column({ unique: true, nullable: true })
  IDWIZDEO: string

  @ManyToOne(() => Categorie, (categorie) => categorie.YOUTUBERS)
  CAT1: Categorie

  @Column({ nullable: true })
  SOUSCAT1: string

  @ManyToOne(() => Categorie, (categorie) => categorie.YOUTUBERS)
  CAT2: Categorie

  @Column({ nullable: true })
  SOUSCAT2: string

  @Column({ nullable: true })
  SEXE: string
}
