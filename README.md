# YoutubeDex Tindex Server

[Spreadsheets avec les données](https://docs.google.com/spreadsheets/d/1il0h8DEtB5i4jIjurFzB_cpVf1swMIwT5R3mOnMkNKk/edit#gid=0)

[Cachier des charges](https://docs.google.com/document/d/1uvP2IoktxF9xMp2A1yZaOQWSV2sQ_dg2CVoecRoDq7s/edit#)

Dans la bdd des youtubers:

| Nom de la colone | "Type" de la colone | Attributs         |
| ---------------- | ------------------- | ----------------- |
| ID               | VARCHAR(255)        | @unique           |
| VALIDATE         | TINYINT(4)          | @default(false)   |
| URLID            | VARCHAR(255)        | @unique           |
| URLPSEUDO        | VARCHAR(255)        | @unique @nullable |
| URLWIZDEO        | VARCHAR(255)        | @unique           |
| IDWIZDEO         | VARCHAR(255)        | @unique @nullable |
| CAT1             | CATEGORIE           | @nullable         |
| SOUSCAT1         | VARCHAR(255)        | @nullable         |
| CAT2             | CATEGORIE           | @nullable         |
| SOUSCAT2         | VARCHAR(225)        | @nullable         |
| SEXE             | VARCHAR(255)        | @nullable         |

> Note: Pour `VALIDATE` 0 => pas controllé, 1 => pas valide, 2 => valide

## Scripts

`npm run init:ytb` Pour initialiser les youtubers. **Prend beaucoup de temps: 742.70s**

`npm run init:cat` Pour initialiser les categories.

## Query

```graphql
query GetData {
  getRandomYoutuber {
    URLPSEUDO
    URLID
    uuid
  }
}

query GetCat {
  getCategories {
    ID
    NAME
  }
}
```

## Mutation

'Query':

```graphql
mutation Validate($input: ValideInput!) {
  validateYoutuber(input: $input) {
    CAT1 {
      NAME
    }
    SOUSCAT1
    CAT2 {
      NAME
    }
    SOUSCAT2
  }
}
```

Variables:

```json
{
  "input": {
    "uuid": "some-uuid",
    "VALIDATE": true,
    "CAT1": 1,
    "SOUSCAT1": "ASMR SW3G",
    "CAT2": 2,
    "SOUSCAT2": "Parodie de films",
    "FEMALE": true
  }
}
```
