"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_yoga_1 = require("graphql-yoga");
var Youtubers_1 = require("./entity/Youtubers");
var Categories_1 = require("./entity/Categories");
var schema_1 = require("./schema");
var connections_1 = require("./connections");
var lambda = new graphql_yoga_1.GraphQLServerLambda({
    schema: schema_1.default,
    context: function (ctx) { return ({
        req: ctx.request,
        Youtuber: Youtubers_1.Youtuber,
        Categorie: Categories_1.Categorie,
    }); },
});
exports.graphqlHandler = function (event, context, callback) {
    if (process.env.NODE_ENV === 'production') {
        connections_1.prodConnection().then(function () { return lambda.graphqlHandler(event, context, callback); });
    }
    else {
        connections_1.devConnection().then(function () { return lambda.graphqlHandler(event, context, callback); });
    }
};
exports.playgroundHandler = lambda.playgroundHandler;
// export const graphqlHandler = lambda.graphqlHandler
