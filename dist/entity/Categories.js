"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Youtubers_1 = require("./Youtubers");
var Categorie = /** @class */ (function (_super) {
    __extends(Categorie, _super);
    function Categorie() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Categorie.prototype, "ID", void 0);
    __decorate([
        typeorm_1.Column({ unique: true }),
        __metadata("design:type", String)
    ], Categorie.prototype, "NAME", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Youtubers_1.Youtuber; }, function (youtuber) { return youtuber.CAT1; }),
        typeorm_1.OneToMany(function (type) { return Youtubers_1.Youtuber; }, function (youtuber) { return youtuber.CAT2; }),
        __metadata("design:type", Array)
    ], Categorie.prototype, "YOUTUBERS", void 0);
    Categorie = __decorate([
        typeorm_1.Entity()
    ], Categorie);
    return Categorie;
}(typeorm_1.BaseEntity));
exports.Categorie = Categorie;
