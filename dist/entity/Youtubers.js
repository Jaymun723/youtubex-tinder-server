"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var Categories_1 = require("./Categories");
var Youtuber = /** @class */ (function (_super) {
    __extends(Youtuber, _super);
    function Youtuber() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    __decorate([
        typeorm_1.PrimaryColumn({ nullable: false, unique: true }),
        typeorm_1.Generated('uuid'),
        __metadata("design:type", String)
    ], Youtuber.prototype, "uuid", void 0);
    __decorate([
        typeorm_1.Column('int', { default: 0 }),
        __metadata("design:type", Number)
    ], Youtuber.prototype, "VALIDATE", void 0);
    __decorate([
        typeorm_1.Column({ unique: true }),
        __metadata("design:type", String)
    ], Youtuber.prototype, "URLID", void 0);
    __decorate([
        typeorm_1.Column({ unique: true, nullable: true }),
        __metadata("design:type", String)
    ], Youtuber.prototype, "URLPSEUDO", void 0);
    __decorate([
        typeorm_1.Column({ unique: true }),
        __metadata("design:type", String)
    ], Youtuber.prototype, "URLWIZDEO", void 0);
    __decorate([
        typeorm_1.Column({ unique: true, nullable: true }),
        __metadata("design:type", String)
    ], Youtuber.prototype, "IDWIZDEO", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Categories_1.Categorie; }, function (categorie) { return categorie.YOUTUBERS; }),
        __metadata("design:type", Categories_1.Categorie)
    ], Youtuber.prototype, "CAT1", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], Youtuber.prototype, "SOUSCAT1", void 0);
    __decorate([
        typeorm_1.ManyToOne(function () { return Categories_1.Categorie; }, function (categorie) { return categorie.YOUTUBERS; }),
        __metadata("design:type", Categories_1.Categorie)
    ], Youtuber.prototype, "CAT2", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], Youtuber.prototype, "SOUSCAT2", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], Youtuber.prototype, "SEXE", void 0);
    Youtuber = __decorate([
        typeorm_1.Entity()
    ], Youtuber);
    return Youtuber;
}(typeorm_1.BaseEntity));
exports.Youtuber = Youtuber;
