"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var url_1 = require("url");
exports.reqChannelBaseUrl = function () {
    var url = new url_1.URL('https://www.googleapis.com/youtube/v3/channels');
    url.searchParams.append('key', process.env.YOUTUBE_API_KEY);
    return url;
};
exports.reqPlayListItemlBaseUrl = function () {
    var url = new url_1.URL('https://www.googleapis.com/youtube/v3/playlistItems');
    url.searchParams.append('key', process.env.YOUTUBE_API_KEY);
    return url;
};
exports.reqVideoBaseUrl = function () {
    var url = new url_1.URL('https://www.googleapis.com/youtube/v3/videos');
    url.searchParams.append('key', process.env.YOUTUBE_API_KEY);
    return url;
};
