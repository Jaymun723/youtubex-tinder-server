"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var events_1 = require("events");
var Papa = require("papaparse");
var Youtubers_1 = require("../entity/Youtubers");
var connections_1 = require("../connections");
var dotEnv = require("dotenv");
function main() {
    return __awaiter(this, void 0, void 0, function () {
        var file;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (!(process.argv[2] === 'prod')) return [3 /*break*/, 2];
                    dotEnv.config({ path: '.env.prod' });
                    return [4 /*yield*/, connections_1.prodConnection('src', '.ts')];
                case 1:
                    _a.sent();
                    return [3 /*break*/, 4];
                case 2: return [4 /*yield*/, connections_1.devConnection()];
                case 3:
                    _a.sent();
                    _a.label = 4;
                case 4:
                    file = fs.readFileSync(path.resolve(__dirname, '../../data/chaine.csv'), { encoding: 'UTF8' });
                    Papa.parse(file, {
                        complete: function (_a) {
                            var data = _a.data;
                            var emiter = new events_1.EventEmitter();
                            var youtubers = data.slice(1, data.length);
                            // const youtubers = data.slice(1, 10)
                            var savedYtb = 0;
                            youtubers.forEach(function (youtuber) {
                                var ytb = new Youtubers_1.Youtuber();
                                ytb.URLID = youtuber[0];
                                youtuber[1].length !== 0 && (ytb.URLPSEUDO = youtuber[1]);
                                ytb.URLWIZDEO = youtuber[2];
                                youtuber[3].length !== 0 && (ytb.IDWIZDEO = youtuber[3]);
                                ytb
                                    .save()
                                    .then(function () { return emiter.emit('save'); })
                                    .catch(function (err) { return console.error('Error !'); });
                            });
                            emiter.on('save', function () {
                                savedYtb++;
                                if (savedYtb + 1 === youtubers.length) {
                                    console.log('Finished !');
                                    process.exit(0);
                                }
                            });
                        },
                    });
                    return [2 /*return*/];
            }
        });
    });
}
main();
