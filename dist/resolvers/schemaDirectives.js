"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var graphql_tools_1 = require("graphql-tools");
var graphql_1 = require("graphql");
// import { Context } from '../utils'
var AuthDirective = /** @class */ (function (_super) {
    __extends(AuthDirective, _super);
    function AuthDirective() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AuthDirective.prototype.visitFieldDefinition = function (field) {
        var _this = this;
        var _a = field.resolve, resolve = _a === void 0 ? graphql_1.defaultFieldResolver : _a;
        field.resolve = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            // console.log(args[2])
            return resolve.apply(_this, args);
            // if (args[2].headers.authorization && args[2].headers.authorization === process.env.ENDPOINT_PASSWORD) {
            //   return resolve.apply(this, args)
            // } else {
            //   throw new Error('Not authorized.')
            // }
        };
    };
    return AuthDirective;
}(graphql_tools_1.SchemaDirectiveVisitor));
exports.AuthDirective = AuthDirective;
var DelayDirective = /** @class */ (function (_super) {
    __extends(DelayDirective, _super);
    function DelayDirective() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DelayDirective.prototype.visitFieldDefinition = function (field) {
        var _this = this;
        var _a = field.resolve, resolve = _a === void 0 ? graphql_1.defaultFieldResolver : _a;
        field.resolve = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            return new Promise(function (r) {
                setTimeout(function () {
                    r(resolve.apply(_this, args));
                }, _this.args.time);
            });
        };
    };
    return DelayDirective;
}(graphql_tools_1.SchemaDirectiveVisitor));
exports.DelayDirective = DelayDirective;
