"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var node_fetch_1 = require("node-fetch");
// import { Youtuber } from '../entity/Youtubers'
// import { Categorie } from '../entity/Categories'
var utils_1 = require("../utils/utils");
var YoutubersError = /** @class */ (function (_super) {
    __extends(YoutubersError, _super);
    function YoutubersError() {
        var _this = _super.call(this, 'No more youtubers to validate !') || this;
        _this.name = 'YoutubersError';
        return _this;
    }
    return YoutubersError;
}(Error));
var resolvers = {
    Query: {
        version: function () { return '1.2.1'; },
        getRandomYoutuber: function (p, a, _a) {
            var Youtuber = _a.Youtuber;
            return __awaiter(_this, void 0, void 0, function () {
                var ytb;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, Youtuber.findOne({ where: { VALIDATE: 0 } })];
                        case 1:
                            ytb = _a.sent();
                            if (!ytb) {
                                throw new YoutubersError();
                            }
                            return [2 /*return*/, ytb];
                    }
                });
            });
        },
        getCategories: function (p, a, _a) {
            var Categorie = _a.Categorie;
            return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                return [2 /*return*/, Categorie.find()];
            }); });
        },
        getChannelSnippet: function (_, _a) {
            var id = _a.id;
            return __awaiter(_this, void 0, void 0, function () {
                var reqUrl, res, data, snippet;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            reqUrl = utils_1.reqChannelBaseUrl();
                            reqUrl.searchParams.append('part', 'snippet');
                            reqUrl.searchParams.append('id', id);
                            return [4 /*yield*/, node_fetch_1.default(reqUrl.toString())];
                        case 1:
                            res = _a.sent();
                            return [4 /*yield*/, res.json()];
                        case 2:
                            data = _a.sent();
                            if (data.pageInfo.totalResults !== 1) {
                                throw new Error('Invalid ID provided.');
                            }
                            snippet = data.items[0].snippet;
                            return [2 /*return*/, {
                                    title: snippet.title,
                                    description: snippet.description,
                                    thumbnails: snippet.thumbnails.high.url,
                                }];
                    }
                });
            });
        },
        getChannelVideos: function (_, _a) {
            var id = _a.id;
            return __awaiter(_this, void 0, void 0, function () {
                var _this = this;
                var reqUrlChannel, resChannel, dataChannel, uploads, reqUrlPlaylistItems, resPlaylist, dataPlaylist, vidsId, videos;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            reqUrlChannel = utils_1.reqChannelBaseUrl();
                            reqUrlChannel.searchParams.append('part', 'contentDetails');
                            reqUrlChannel.searchParams.append('id', id);
                            return [4 /*yield*/, node_fetch_1.default(reqUrlChannel.toString())];
                        case 1:
                            resChannel = _a.sent();
                            return [4 /*yield*/, resChannel.json()];
                        case 2:
                            dataChannel = _a.sent();
                            if (dataChannel.pageInfo.totalResults !== 1) {
                                throw new Error('Invalid ID provided.');
                            }
                            uploads = dataChannel.items[0].contentDetails.relatedPlaylists.uploads;
                            reqUrlPlaylistItems = utils_1.reqPlayListItemlBaseUrl();
                            reqUrlPlaylistItems.searchParams.append('part', 'contentDetails');
                            reqUrlPlaylistItems.searchParams.append('playlistId', uploads);
                            reqUrlPlaylistItems.searchParams.append('maxResults', '10');
                            return [4 /*yield*/, node_fetch_1.default(reqUrlPlaylistItems.toString())];
                        case 3:
                            resPlaylist = _a.sent();
                            return [4 /*yield*/, resPlaylist.json()];
                        case 4:
                            dataPlaylist = _a.sent();
                            if (dataPlaylist.pageInfo.totalResults === 0) {
                                throw new Error("This channel don't have any videos...");
                            }
                            vidsId = dataPlaylist.items.map(function (item) { return item.contentDetails.videoId; });
                            videos = vidsId.map(function (vidId) { return __awaiter(_this, void 0, void 0, function () {
                                var reqUrlVideo, resVideo, dataVideo, video;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            reqUrlVideo = utils_1.reqVideoBaseUrl();
                                            reqUrlVideo.searchParams.append('part', 'statistics,snippet');
                                            reqUrlVideo.searchParams.append('id', vidId);
                                            return [4 /*yield*/, node_fetch_1.default(reqUrlVideo.toString())];
                                        case 1:
                                            resVideo = _a.sent();
                                            return [4 /*yield*/, resVideo.json()];
                                        case 2:
                                            dataVideo = _a.sent();
                                            video = dataVideo.items[0];
                                            return [2 /*return*/, {
                                                    id: vidId,
                                                    title: video.snippet.title,
                                                    description: video.snippet.description,
                                                    publishedAt: video.snippet.publishedAt,
                                                    thumbnails: video.snippet.thumbnails.medium.url,
                                                    url: "https://www.youtube.com/watch?v=" + vidId,
                                                    views: video.statistics.viewCount,
                                                    likes: video.statistics.likeCount,
                                                    dislikes: video.statistics.dislikeCount,
                                                }];
                                    }
                                });
                            }); });
                            return [2 /*return*/, videos];
                    }
                });
            });
        },
    },
    Mutation: {
        validateYoutuber: function (_, _a, _b, info) {
            var args = _a.input;
            var Youtuber = _b.Youtuber, Categorie = _b.Categorie;
            return __awaiter(_this, void 0, void 0, function () {
                var ytb, CAT1, CAT2;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, Youtuber.findOne(args.uuid)];
                        case 1:
                            ytb = _a.sent();
                            if (!ytb) {
                                throw new Error('No youtuber found...');
                            }
                            if (args.VALIDATE === false) {
                                ytb.VALIDATE = 1;
                                ytb.save();
                                return [2 /*return*/, ytb];
                            }
                            return [4 /*yield*/, Categorie.findOne(args.CAT1)];
                        case 2:
                            CAT1 = _a.sent();
                            if (!CAT1) {
                                throw new Error("No categorie found for id " + args.CAT1 + "...");
                            }
                            if (!args.CAT2) return [3 /*break*/, 4];
                            return [4 /*yield*/, Categorie.findOne(args.CAT2)];
                        case 3:
                            CAT2 = _a.sent();
                            if (!CAT2) {
                                throw new Error("No categorie found for id " + args.CAT2 + "...");
                            }
                            ytb.CAT2 = CAT2;
                            _a.label = 4;
                        case 4:
                            ytb.VALIDATE = 2;
                            ytb.CAT1 = CAT1;
                            args.SOUSCAT1 && (ytb.SOUSCAT1 = args.SOUSCAT1);
                            args.SOUSCAT2 && (ytb.SOUSCAT2 = args.SOUSCAT2);
                            ytb.SEXE = args.FEMALE === true ? 'féminin' : args.FEMALE === false ? 'masculin' : ytb.SEXE;
                            return [4 /*yield*/, ytb.save()];
                        case 5:
                            _a.sent();
                            return [2 /*return*/, ytb];
                    }
                });
            });
        },
    },
};
exports.default = resolvers;
